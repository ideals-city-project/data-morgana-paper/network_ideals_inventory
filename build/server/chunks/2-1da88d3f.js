const index = 2;
let component_cache;
const component = async () => component_cache ??= (await import('./_page.svelte-823c8c50.js')).default;
const imports = ["_app/immutable/nodes/2.4792be00.js","_app/immutable/chunks/index.f284bd3d.js","_app/immutable/chunks/index.253c8e76.js"];
const stylesheets = ["_app/immutable/assets/2.fbb7cc82.css"];
const fonts = [];

export { component, fonts, imports, index, stylesheets };
//# sourceMappingURL=2-1da88d3f.js.map
