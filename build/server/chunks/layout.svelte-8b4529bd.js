import { c as create_ssr_component } from './index2-48f00034.js';

const Layout = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  return `${slots.default ? slots.default({}) : ``}`;
});

export { Layout as default };
//# sourceMappingURL=layout.svelte-8b4529bd.js.map
