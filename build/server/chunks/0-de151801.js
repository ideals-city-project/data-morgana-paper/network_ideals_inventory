const index = 0;
let component_cache;
const component = async () => component_cache ??= (await import('./layout.svelte-8b4529bd.js')).default;
const imports = ["_app/immutable/nodes/0.2ccec392.js","_app/immutable/chunks/index.f284bd3d.js"];
const stylesheets = [];
const fonts = [];

export { component, fonts, imports, index, stylesheets };
//# sourceMappingURL=0-de151801.js.map
