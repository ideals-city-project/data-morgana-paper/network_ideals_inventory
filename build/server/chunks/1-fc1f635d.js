const index = 1;
let component_cache;
const component = async () => component_cache ??= (await import('./error.svelte-9d1833b6.js')).default;
const imports = ["_app/immutable/nodes/1.51b202e7.js","_app/immutable/chunks/index.f284bd3d.js","_app/immutable/chunks/singletons.f5ddafee.js","_app/immutable/chunks/index.253c8e76.js"];
const stylesheets = [];
const fonts = [];

export { component, fonts, imports, index, stylesheets };
//# sourceMappingURL=1-fc1f635d.js.map
